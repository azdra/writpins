import React from 'react';
import {IonContent} from "@ionic/react";
import PinsListItems from "@components/PinsListItems";
import PinsButtonAdd from "@components/PinsAdd/PinsButtonAdd";

const PinsList = () => {
    const [pins, setPins] = React.useState([
        {
            id: 1,
            title: 'Une mine riche',
            description: 'Considérez l\'homme comme une mine riche en pierres précieuse de toutes sortes. L\'éducation peut, seule, en faire l\'usage qu\'elle veut.',
            tags: ['education', 'interiorite']
        },
        {
            id: 2,
            title: 'La véracité',
            description: 'La véracité est la vertu des hommes forts.',
            tags: ['veracite', 'force']
        },
        {
            id: 2,
            title: 'Sans préjugés',
            description: 'Il faut se garder de juger les hommes sur ce qu\'ils ignorent et de les mépriser pour ce qu\'ils savent.',
            tags: ['jugement', 'ignorance']
        }
    ]);

    const addPins = (pin) => {
        pin.id = pins.length + 1;
        setPins([...pins, pin]);
    }


    return (
        <IonContent>
            {
                pins.map((pin, index) => {
                    return <PinsListItems key={index} {...pin}/>
                })
            }

            <PinsButtonAdd addPins={addPins}/>
        </IonContent>
    );
}

export default PinsList;