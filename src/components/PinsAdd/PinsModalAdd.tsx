import {
    IonButton, IonButtons,
    IonContent,
    IonHeader,
    IonInput, IonItem,
    IonModal, IonText, IonTitle,
    IonToolbar
} from "@ionic/react";
import React, {useRef} from "react";
import {OverlayEventDetail} from "@ionic/core";

const PinsModalAdd = (props) => {
    const modal = useRef<HTMLIonModalElement | null>(null);
    const title = useRef<HTMLIonInputElement | null>(null);
    const description = useRef<HTMLIonInputElement | null>(null);
    const tags = useRef<HTMLIonInputElement | null>(null);

    const confirm = (e) => {
        e.preventDefault();
        console.log('Confirm Ok')
        modal.current?.dismiss({
            title, description, tags
        }, 'confirm');
    }

    const onWillDismiss = (ev: CustomEvent<OverlayEventDetail>) => {
        if (ev.detail.role === 'confirm') {
            console.log(ev.detail.data)
            props.addPins({
                title: ev.detail.data.title.current.value,
                description: ev.detail.data.description.current.value,
                tags: ev.detail.data.tags.current.value.split(';')
            })
        }
    }

    return (
        <>
            <IonModal ref={modal} trigger="open-modal" onWillDismiss={(ev) => onWillDismiss(ev)}>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonButton onClick={() => modal.current?.dismiss()}>Cancel</IonButton>
                        </IonButtons>
                        <IonTitle>Créer une épingle</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    <form action="" onSubmit={confirm}>
                        <IonItem>
                            <IonInput ref={title} labelPlacement="floating">
                                <div slot="label">
                                    Titre <IonText color="danger">(Required)</IonText>
                                </div>
                            </IonInput>
                        </IonItem>

                        <IonItem>
                            <IonInput ref={description} labelPlacement="floating">
                                <div slot="label">
                                    Description <IonText color="danger">(Required)</IonText>
                                </div>
                            </IonInput>
                        </IonItem>
                        <IonItem>
                            <IonInput helperText="Séparer chaque tags par un ;" ref={tags} labelPlacement="floating">
                                <div slot="label">
                                    Tags <IonText color="danger">(Required)</IonText>
                                </div>
                            </IonInput>
                        </IonItem>
                        <IonButton expand="block" type={"submit"}>Ajouter</IonButton>
                    </form>
                </IonContent>
            </IonModal>
        </>
    )
}
export default PinsModalAdd;