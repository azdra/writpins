export interface IPinsListItems {
    id: number
    title: string
    description: string
    tags: string[]
}