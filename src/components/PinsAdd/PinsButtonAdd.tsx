import {
    IonFab,
    IonFabButton,
    IonIcon
} from "@ionic/react";
import {add} from "ionicons/icons";
import React from "react";
import PinsModalAdd from "@components/PinsAdd/PinsModalAdd";

const PinsButtonAdd = (props) => {
    return (
        <>
            <PinsModalAdd addPins={props.addPins}/>

            <IonFab id="open-modal" vertical="bottom" horizontal="end">
                <IonFabButton>
                    <IonIcon icon={add}></IonIcon>
                </IonFabButton>
            </IonFab>
        </>
    )
}
export default PinsButtonAdd;