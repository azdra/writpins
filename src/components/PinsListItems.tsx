import React from 'react';
import {
    IonBadge,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonContent,
    IonItem, useIonRouter
} from "@ionic/react";
import {IPinsListItems} from "@components/PinsAdd/IPinsListItems";

const PinsListItems = (props: IPinsListItems) => {

    const changePage = () => {
        const router = useIonRouter();
        router.push('/pins/' + props.id);
    }

    return (
        <IonCard onClick={changePage}>
            <IonCardHeader>
                <IonCardTitle>{props.title}</IonCardTitle>
            </IonCardHeader>

            <IonCardContent>{props.description}</IonCardContent>

            <IonItem>
                {
                    (props.tags ?? []).map((tag, index) => {
                        return <IonBadge style={{margin: 2}} slot="end" key={index} color="primary"># {tag}</IonBadge>
                    })
                }
            </IonItem>
        </IonCard>
    );
}

export default PinsListItems;