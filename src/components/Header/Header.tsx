import React from "react"
import {FC} from "react";
import {
    IonButtons,
    IonContent,
    IonHeader, IonIcon, IonItem, IonLabel, IonList,
    IonMenu, IonMenuButton, IonPage,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import {logoIonic, pinOutline} from "ionicons/icons";

const Header: FC = () => {
    return (
        <>
            <IonMenu contentId={"main-content"}>
                <IonHeader>
                    <IonToolbar>
                        <IonTitle slot={"start"}>Menu</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className={"ion-padding"}>
                    <IonList lines="full">
                        <IonItem>
                            <IonLabel>Mes épingles</IonLabel>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Thématiques</IonLabel>
                        </IonItem>
                        <IonItem>
                            <IonLabel>A propos</IonLabel>
                        </IonItem>
                    </IonList>
                </IonContent>
            </IonMenu>
            <IonHeader id={"main-content"}>
                <IonToolbar>
                    <IonButtons slot={"start"}>
                        <IonMenuButton></IonMenuButton>
                    </IonButtons>
                    <IonIcon slot={"start"} icon={pinOutline}></IonIcon>
                    <IonTitle class="ion-text-center">Writ'Pins</IonTitle>
                </IonToolbar>
            </IonHeader>
        </>
    )
}

export default Header