import React from 'react';
import {FC} from "react";
import {IonReactRouter} from "@ionic/react-router";
import {IonRouterOutlet} from "@ionic/react";
import {Route} from "react-router-dom";
import HomeScreen from "@screens/HomeScreen";
import pinsDetailScreen from "@screens/PinsDetailScreen";

const AppRouter: FC = () => {
    return <>
        <IonReactRouter>
            <IonRouterOutlet>
                <Route path="/" component={HomeScreen} exact={true} />
                <Route path="/pins/:pinId" component={pinsDetailScreen} exact={true} />
            </IonRouterOutlet>
        </IonReactRouter>
    </>
};

export default AppRouter;
