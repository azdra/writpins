import React, {FC} from 'react';
import Header from "@components/Header/Header";
import {IonPage} from "@ionic/react";
import PinsButtonAdd from "@components/PinsAdd/PinsButtonAdd";
import PinsList from "@components/PinsList";

const HomeScreen : FC = () => {
    return <>
        <IonPage>
            <Header/>
            <PinsList/>
        </IonPage>
    </>
};

export default HomeScreen;