import React from 'react';
import {IonApp, setupIonicReact} from "@ionic/react";
import {createRoot} from 'react-dom/client';

import '@ionic/react/css/core.css';
import '@scss/index.scss';
import AppRouter from "@app/Router/AppRouter";

setupIonicReact();

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
    <IonApp>
        <AppRouter/>
    </IonApp>
);
